import {authClient} from '../utils/fauna-client'
import {query as q} from 'faunadb'

export default async function ({ store, app }) {
	console.log("Authenticating")
	const token = app.$cookiz.get('token')

	if(!token) {
		console.log("NOT LOGGED IN YET")
	}
}
