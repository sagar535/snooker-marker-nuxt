// import Vue from 'vue'
import Vuex from "vuex"
import {query as q} from "faunadb"
import {gql} from 'graphql-request'
import {authClient} from '../utils/fauna-client'
import {graphQLClient} from '../utils/graphql-client'
import Cookies from 'js-cookie'

// Vue.use(Vuex)

// new Vuex.Store({

// })
export const state = () => ({
  turn: 1,
  score1: 0,
  score2: 0,
  balls1: [],
  balls2: [],
  fouls1: [],
  fouls2: [],
  breaks1: [],
  breaks2: [],
  continue_break: true,
  red_on: true,
  color_on: false,
  last_color_ball_on: false,
  current_frame_id: null,
  overlay: false,
  player_1_name: "Player 1",
  player_2_name: "Player 2"
})

export const mutations = {
    setStateFromFrame: (state, {frame, frame_id}) => {
      console.log('state', state)
      console.log('frame', frame)
      console.log('frame id', frame_id)
      state.turn = frame.turn
      state.score1 = frame.score1
      state.score2 = frame.score2
      state.balls1 = frame.balls1
      state.balls2 = frame.balls2
      state.fouls1 = frame.fouls1
      state.fouls2 = frame.fouls2
      state.breaks1 = frame.breaks1
      state.breaks2 = frame.breaks2
      state.continue_break = frame.continue_break
      state.red_on = frame.red_on
      state.color_on = frame.color_on
      state.last_color_ball_on = frame.last_color_ball_on
      state.player_1_name = frame.player_1_name || "Player 1"
      state.player_2_name = frame.player_2_name || "Player 2"

      state.current_frame_id = frame_id

      console.log("setFrameIDState", state)
    },
    switch_players: state => {
        state.turn == 1 ? state.turn = 2 : state.turn = 1
        state.continue_break = false
        state.red_on = true
        state.color_on = false
    },
    switch_to_player : (state, player) => {
      state.turn = player
      state.continue_break = false
      if(!state.last_color_ball_on) {
        state.red_on = true
        state.color_on = false
      }
    },
    pot: (state, ball) => {
      if(state.turn == 1) {
        state.balls1.push(ball)
      }
      else {
        state.balls2.push(ball)
      }

      var breaks = (state.turn == 1 ? state.breaks1[state.breaks1.length-1] : state.breaks2[state.breaks2.length-1])

      // check if break has stated yet if not insert new break or if new break is about to begin
      if (typeof(breaks) == 'undefined' || !state.continue_break){
        if (state.turn == 1) {
          state.breaks1.push([ball])
        } else {
          state.breaks2.push([ball])
        }
        // [TODO:] in such case the ball is usually 1, we can write some sort of test later
        // so now color should be on
        state.color_on = true
        state.red_on = false
      }
      // if in middle of break insert points to last break
      else if(state.continue_break) {
        if (state.turn == 1) {
          state.breaks1[state.breaks1.length-1].push(ball)
        } else {
          state.breaks2[state.breaks2.length-1].push(ball)
        }
        // in middle of break we should check for the last ball to be color or red
        if(ball == 1) {
          state.color_on = true
          state.red_on = false
        }else if(state.last_color_ball_on == true){
          state.color_on = true
          state.red_on = false
        } else {
          state.red_on = true
          state.color_on = false
        }

      }
      state.continue_break = true
    },
    foul: (state, foul_point) => {
      if (state.turn == 1) {
        state.fouls1.push(foul_point)
      }else {
        state.fouls2.push(foul_point)
      }
      state.continue_break = false
    },
    recalculate_scores: (state, getters) => {
      state.score1 = getters.compute_score1
      state.score2 = getters.compute_score2
    },
    last_color_ball_on: (state, status) => {
      state.color_on = status
      state.red_on = (!status)
      state.last_color_ball_on = status
    },
    setOverlay: (state, value) => {
      state.overlay = value
    },
    saveNames: (state, {player1, player2}) => {
      state.player_1_name = player1
      state.player_2_name = player2
    }
}

export const getters = {
  compute_score1: state => {
    var ball_point = state.balls1.length > 0 ? state.balls1.reduce((a,b) => a + b) : 0
    var foul_point = state.fouls2.length > 0 ? state.fouls2.reduce((a,b) => a + b) : 0
    return ball_point + foul_point
  },
  compute_score2: state => {
    var ball_point = state.balls2.length > 0 ? state.balls2.reduce((a,b) => a + b) : 0
    var foul_point = state.fouls1.length > 0 ? state.fouls1.reduce((a,b) => a + b) : 0
    return ball_point + foul_point
  },
  whose_turn: state => { return state.turn == 1 ? "Player 1" : "Player 2" },
  default_frame: state => ({
    turn: 1,
    score1: 0,
    score2: 0,
    balls1: [],
    balls2: [],
    fouls1: [],
    fouls2: [],
    breaks1: [],
    breaks2: [],
    continue_break: true,
    red_on: true,
    color_on: false,
    last_color_ball_on: false,
    player_1_name: "Player 1",
    player_2_name: "Player 2",
  })
}

export const actions = {
  setOverlay: ({commit}, value) => {
    commit('setOverlay', value)
  },
  switch_players: ({commit, dispatch}) => {
    commit('switch_players')
    dispatch('saveStateOnline')
  },
  pot: ({commit, getters, dispatch, state}, ball) => {
    commit('pot', ball)
    commit('recalculate_scores', getters)
    dispatch('saveStateOnline')
  },
  foul: ({commit, getters, dispatch}, point) => {
    commit('foul', point)
    commit('recalculate_scores', getters)
    dispatch('saveStateOnline')
  },
  switch_to_player: ({commit, dispatch, state}, player) => {
    commit('switch_to_player', player)
    dispatch('saveStateOnline')
  },
  last_color_ball_on: ({commit, dispatch, state}, status) => {
    commit('last_color_ball_on', status)
    dispatch('saveStateOnline')
  },
  setStateFromFrame: ({commit}, {frame, frame_id}) => {
    console.log("setting StateFromFrame")
    commit('setStateFromFrame', {frame, frame_id})
  },
  saveStateOnline: async ({state}) => {
    const tempState = state
    delete tempState['authenticated']
    const onlineState = JSON.stringify(tempState)
    const token = Cookies.get('token')

    let user_id

    if(token) {
      try {
        const { ref, data } = await authClient(token).query(q.Get(q.Identity()));
        user_id = ref.id
      } catch (error) {
        console.error('user fetch error' ,error);
      }

      console.log('current frame id', state.current_frame_id)

      const query = gql`
      mutation updateAFrame(
        $id: ID!
        $turn: Float!
        $score1: Float!
        $score2: Float!
        $balls1: [Float!]
        $balls2: [Float!]
        $fouls1: [Float!]
        $fouls2: [Float!]
        $breaks1: [[Float!]]
        $breaks2: [[Float!]]
        $continue_break: Boolean!
        $red_on: Boolean!
        $color_on: Boolean!
        $last_color_ball_on: Boolean!
        $player_1_name: String!
        $player_2_name: String!
      ) {
        updateFrame(id: $id, data: {
          turn: $turn,
          score1: $score1,
          score2: $score2,
          balls1: $balls1,
          balls2: $balls2,
          fouls1: $fouls1,
          fouls2: $fouls2,
          breaks1: $breaks1,
          breaks2: $breaks2,
          continue_break: $continue_break,
          red_on: $red_on,
          color_on: $color_on,
          last_color_ball_on: $last_color_ball_on,
          player_1_name: $player_1_name,
          player_2_name: $player_2_name
        }) {
          turn
          score1
          score2
          continue_break
          red_on
          color_on
          last_color_ball_on
          balls1
          balls2
          breaks1
          breaks2
          fouls1
          fouls2
          player_1_name
          player_2_name
        }
      }
    `

      try {
        await graphQLClient(token)
          .request(query, Object.assign({id: state.current_frame_id}, JSON.parse(onlineState)));
      } catch (error) {
        console.error('gqp caught error', error);
      }
    }
  },
  saveNames: ({commit}, {player1, player2}) => {
    commit("saveNames", {player1, player2})
  }
}

// const store = new Vuex.Store({
// })

// export default store
