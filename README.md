# snooker-marker-nuxt

## DB setup

```
- sign up in faunadb.com if you don't have an account yet
- go to dashboard.faunadb.com
- create new database , name it as you want like snooker-marker
- upload schema.gql 
- create a guest role
- add collection user, with privileges read and create
- add index user_by_email and save 
- create a key with role guest
- create a auth role
- add collection user with read privilege
- add collection frame with READ, WRITE, CREATE, DELETE privileges
- edit for each privilege, just uncomment the lines there
- add index allFrames
- No need to create key for auth role, once user is authenticated a auth token is generated and sent to client
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
