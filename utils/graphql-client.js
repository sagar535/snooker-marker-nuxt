import { GraphQLClient } from 'graphql-request';

const endpoint = 'https://graphql.fauna.com/graphql';

export const graphQLClient = (token) => {
  const secret = (token || process.env.GUEST_SECRET || process.env.NUXT_ENV_GUEST_SECRET);

  return new GraphQLClient(endpoint, {
    headers: {
      authorization: `Bearer ${secret}`,
      // 'X-Schema-Preview': 'partial-update-mutation', // move to `pages/index.js`
    },
  });
};
