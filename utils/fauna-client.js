import faunadb from 'faunadb';

export const guestClient = new faunadb.Client({
  secret: (process.env.GUEST_SECRET || process.env.NUXT_ENV_GUEST_SECRET),
});

export const authClient = (secret) =>
  new faunadb.Client({
    secret,
  });
