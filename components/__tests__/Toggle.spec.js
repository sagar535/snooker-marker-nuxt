import { shallowMount, createLocalVue } from "@vue/test-utils";
import Toggle from "../Toggle";
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)

const factory = (localVue, vuetify, propsData) => {
  return shallowMount(Toggle, {
  	localVue,
  	vuetify,
  	propsData: propsData
  });
};

describe("Toggle ", () => {
	let localVue, vuetify, propsData;
	let wrapper;
	const label = "Color Ball On"

	beforeEach(() => {
		localVue = createLocalVue();
	    vuetify = new Vuetify();
	    propsData = {label: label, checked: false}

	    wrapper = factory(localVue, vuetify, propsData)
	})

	it("mounts properly", () => {
	    expect(wrapper.vm).toBeTruthy();
    });

    it("should have text label", () => {
    	expect(wrapper.text()).toMatch(/Color Ball On/)
    })

    it("should contain the switch", () => {
    	expect(wrapper.find('span.switch').exists()).toBe(true)
    })

    it("should not be checked", () => {
    	expect(wrapper.find('input').checked).toBeFalsy()
    })

    it("emits updateChecked on click", () => {
    	wrapper.find('input').trigger('click');
    	// docs suggesting await is not working so resolved to using timeOut
    	setTimeout(() => { expect(wrapper.emitted().updateChecked).toBeTruthy(); }, 1000)
    })
})
