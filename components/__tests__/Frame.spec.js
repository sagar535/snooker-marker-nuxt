import { shallowMount, mount, createLocalVue } from "@vue/test-utils";
import Frame from "../Frame";
import Toggle from "../Toggle";
import Vuetify from 'vuetify';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuetify).use(Vuex)

const factory = (localVue, vuetify, propsData, store) => {
  return mount(Frame, {
  	localVue,
  	vuetify,
  	propsData: propsData,
  	store,
  	children: [Toggle]
  });
};

describe("Frame", () => {
	let localVue, vuetify, propsData;
	let wrapper;
	let store ;

	beforeEach(() => {
		localVue = createLocalVue();
	    vuetify = new Vuetify();

	    store = new Vuex.Store({
	    	state: {
	    		authenticated: false,
				turn: 1,
				score1: 0,
				score2: 0,
				balls1: [],
				balls2: [],
				fouls1: [],
				fouls2: [],
				breaks1: [],
				breaks2: [],
				continue_break: true,
				red_on: true,
				color_on: false,
				last_color_ball_on: false,
				player_1_name: "Player 1",
			    player_2_name: "Player 2",
	    	}
	    })

	    wrapper = factory(localVue, vuetify, {}, store);

	});

	it("mounts properly", () => {
		expect(wrapper.vm).toBeTruthy();
	});
})
