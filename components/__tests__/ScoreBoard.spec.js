import { shallowMount, createLocalVue } from "@vue/test-utils";
import ScoreBoard from "../ScoreBoard";
import Vuetify from 'vuetify'
import Vue from 'vue'

Vue.use(Vuetify)

const factory = (localVue, vuetify, propsData) => {
  return shallowMount(ScoreBoard, {
  	localVue,
  	vuetify,
  	propsData: propsData
  });
};

describe("ScoreBoard ", () => {
	let localVue, vuetify, propsData;
	let wrapper;

	beforeEach(() => {
		localVue = createLocalVue();
	    vuetify = new Vuetify();
	})

	describe("when player is 1, score 0, selected and no breaks", () => {
		
	  beforeEach(() => {
	    propsData = {
	  		player: 1,
		    score: 0,
		    selected: true,
		    breaks: [],
		    playerName: "Player 1",
	  	};

	  	wrapper = factory(localVue, vuetify, propsData);
	  })

	  it("mounts properly", () => {
	    expect(wrapper.vm).toBeTruthy();
	  });

	  it("should have selected class", () => {
	  	expect(wrapper.classes()).toContain('selected')
	  })

	  it("should contain player 1 score text", () => {
	  	expect(wrapper.text()).toMatch(/Player 1 score is 0/)
	  })

	  // Tests no breaks are displayed when breaks = []
	  it("should not contain text Break", () => {
	  	expect(wrapper.text()).not.toMatch(/Break/)
	  })

	  it("should not contain div with class point-ball", () => {
	  	expect(wrapper.find('div.point-ball').exists()).toBe(false);
	  })
	})

	describe("when player is 2, score 10, not selected and breaks is present", () => {
		beforeEach(() => {
			propsData = {
				player: 2,
				score: 10,
				selected: false,
				breaks: [[1,1,3,1,4]],
				playerName: "Player 2",
			}

			wrapper = factory(localVue, vuetify, propsData)
		})

	  it("should not have selected class", () => {
	  	expect(wrapper.classes()).not.toContain('selected')
	  })

	  it("should contain player 2 score text", () => {
	  	expect(wrapper.text()).toMatch(/Player 2 score is 10/)
	  })

	  // Tests breaks are displayed when breaks is present
	  it("should contain text Break", () => {
	  	expect(wrapper.text()).toMatch(/Break/)
	  })

	  it("should contain div with class point-ball", () => {
	  	expect(wrapper.find('div.point-ball').exists()).toBe(true);
	  })
	})

	describe("get_color", () => {
		const cases = [[1, 'red'],[2, 'yellow'],[3, 'green'],[4, 'brown'],[5, 'blue'],[6, 'pink'],[7, 'black'],[8, 'grey'],[100, 'grey']];
		beforeEach(() => {
		    propsData = {
		  		player: 1,
			    score: 0,
			    selected: true,
			    breaks: []
		  	};
		  	wrapper = factory(localVue, vuetify, propsData);
		});

		it.each(cases)(
			"returns %p, when the point is %p",
			(point, color) => {
				const received_color = wrapper.vm.get_color(point);
				expect(received_color).toBe(color);
			}
		);
	})

	describe("resolve_breaks", () => {
		const pots = [
			[[1,2,1,3,1,4], {"1": 3, "2": 1, "3": 1, "4": 1}], 
			[[1,1,7,1,7,1,7], {"1": 4, "7": 3}], 
			[[1,1,1,1,1,1,5,1,5,1,5], {"1": 8, "5": 3}]
		]
		beforeEach(() => {
			propsData = {
		  		player: 1,
			    score: 0,
			    selected: true,
			    breaks: pots
		  	};
		  	wrapper = factory(localVue, vuetify, propsData);
		})

		it.each(pots)(
			"returns %p when pots are %p", 
			(pot, hash) => {
				expect(wrapper.vm.resolve_breaks(pot)).toMatchObject(hash)
			}
		)
	})
});
