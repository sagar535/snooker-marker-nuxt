import { shallowMount, mount, createLocalVue } from "@vue/test-utils";
import NavBar from "../NavBar";
import Toggle from "../Toggle";
import Vuetify from 'vuetify';
import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';
import faunadb from 'faunadb';

let spy_logout, spy_remote_logout, spy_create_new_frame
jest.mock('js-cookie', () => ({get: () => 'THE_TOKEN'}))

const $router = {
  push: jest.fn(),
}

const $cookiz = {
  get: jest.fn(),
}


Vue.use(Vuetify).use(Vuex)

const factory = (localVue, vuetify, propsData, store) => {
  return mount(NavBar, {
  	localVue,
  	vuetify,
  	propsData: propsData,
  	store,
  	children: [Toggle],
    mocks: {
      $router,
      $cookiz
    },
  });
};

describe("NavBar ", () => {
	let localVue = createLocalVue(), vuetify = new Vuetify(), propsData;
	let wrapper;

	describe('when user is not authenticated', () => {
    let store, state = {last_color_ball_on: false, authenticated: false} ;

    beforeEach(() => {
      store = new Vuex.Store({
        state
      })

      wrapper = factory(localVue, vuetify, {}, store);
    });

    it("mounts properly", () => {
      expect(wrapper.vm).toBeTruthy();
    });

    describe("proper links are displayed", () => {
      it("displays the snooker marker", () => {
        expect(wrapper.text()).toMatch(/Snooker Marker/)
        expect(wrapper.find("a[href='/']").exists()).toBe(true)
      })

      it("displays the offline marker", () => {
        expect(wrapper.text()).toMatch(/Offline/)
        expect(wrapper.find("a[href='/game']").exists()).toBe(true)
      })
    })

    describe("proper buttons are displayed", () => {
      it ('should have signup button', () => {
        const signup = wrapper.find('#signup')
        expect(signup.exists()).toBe(true)
        expect(signup.attributes('href')).toBe('/signup')
      })

      it ('should have login button', () => {
        const login = wrapper.find('#login')
        expect(login.exists()).toBe(true)
        expect(login.attributes('href')).toBe('/login')
      })
    })
  })

  describe('when user is authenticated', () => {
    let state = {last_color_ball_on: false, authenticated: true} ;

    const actions = { logout: jest.fn(), setOverlay: jest.fn() }
    const store = new Vuex.Store({
      state,
      actions
    })

    beforeEach(() => {
      spy_logout = jest.spyOn(NavBar.methods, 'logout')
      spy_remote_logout = jest.spyOn(NavBar.methods, "remote_logout").mockImplementation(() => jest.fn())
      spy_create_new_frame = jest.spyOn(NavBar.methods, 'createNewFrame')
                                  .mockImplementation(() => jest.fn())

      wrapper = factory(localVue, vuetify, {}, store);
    })
    it("should have logout button present", () =>{
      expect(wrapper.find('#logout').exists()).toBe(true)
    })

    it("should have my frames button", () => {
      const frame = wrapper.find("#my-frame")
      expect(frame.exists()).toBe(true)
      expect(frame.attributes('href')).toBe('/frame')
    })

    it('should have Online button', () => {
      const online = wrapper.find('#online')
      expect(online.exists()).toBe(true)
    })

    describe("on logout button click", () => {
      beforeEach(() => {
        const logoutButton = wrapper.find('#logout')
        logoutButton.trigger('click')
      })

      it("should call logout method", () => {
        expect(spy_logout).toHaveBeenCalled()
      })

      it("should query logout", () => {
        expect(spy_remote_logout).toHaveBeenCalled()
      })
    })

    describe('on online button click', () => {
      beforeEach(() => {
        const onlineButton = wrapper.find('#online')
        onlineButton.trigger('click')
      })

      it('should call createNewFrame method', () => {
        expect(spy_create_new_frame).toHaveBeenCalled()
      })
    })
  })
});
